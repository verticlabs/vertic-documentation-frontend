#Development workflow

This is a fairly brief introduction to the workflow, currently employed by the development team at Vertic A/S, Copenhagen.

##Front-end specific development

Our workflow tends to differ, since our projects are highly customised and unique in nature. From that we have evolved a barebone framework, and approach to each project that provided us with a minimal scaffold to build upon.

It includes:

* Common JavaScript starting point, including Require.js
* Build tools powered by Grunt
* Front-end package management powered by Bower 

Optimally, all of our scripts would be built, and obfuscated slightly before any deployment.

##Backend specific development

(...)

##Version control

We make use of Git for version control, which in turn is supported by a feature branching model, to ensure a separation of concerns, during the development of specific features.

Our repositories are hosted by Atlassain (Bitbucket.org), and access is restricted to select members of our team. It happens through global user groups, and on individual repositories.

###Current administrators are:

(In alphabetical order)

* Jesper Bunch (jbunch)
* Johnni Jorgensen (jjorgensen)
* Joshua Wheelock (jwheelock)
* Ole Rud (orud)
* Peter Tving Jensen (pjense
* Rasmus Kalms (rkalms)
* Terji Petersen (terjipedersen)

###Current user groups:

* Administrators
	- Can administer team members, access restrictions and delete repositories.
* Developers
	- Write access to all repositories
* Freelancers
	- Write access to all repositories
* Testers
	- Read access to all repositories
	
*Read more about access restrictions here: [Grant users and groups access](https://confluence.atlassian.com/display/BITBUCKET/Grant+users+and+groups+access) (Bitbucket)*

##Code changes, test and deployment

Deployment depends on what branching model, that are being utilised, but this is the most typical scenario:

A feature is ready for test, so the developer is tasked with merging her changes into *develop*, after initial testing, and from there merging *develop* into *test-release*, depending on what model is being used. From there, the current version will be uploaded to the test server, and any iterations based on that will be made accordingly.

When the feature is ready for deployment, a developer will merge the changes into the *master* branch, update the current tags and deploy to the live environment.

*Read more about our feature branching model here: [Git workflow at Vertic](https://bytebucket.org/verticlabs/vertic-documentation/raw/0c7a014f668f45b497aa8bff9fe6a3f5a658d2c9/docs/git_process_20140606.pdf)*

### Access restrictions on live environment








	 


