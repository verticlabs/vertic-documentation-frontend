# Handover when starting a new project

We developers love the "new project" smell, and we delight in building something new and fresh that can stand on the shoulders of previous projects. It gives us a chance to use our accumulated experiences, and make everyhing just a little bit better for the next big thing.

In the spirit of this feeling, there's a few things designers and project managers should look out for, before assuming that a project is ready to be handed over to your favourite developer.

## The handover

No project are the same, even though similarities are everywhere. So one shouldn't try to create a "one size fits all" type of approach. That being said, there are still a few basic things that one needs to contemplate before going into production.

	User flow and experience: Is there a wireframe present, or does flow documentation consist of different designs in Photoshop?

This is important since the presence of a complete wireframe, can greatly minimize the need for individual page designs.

The other (often) used approach, where a design is communicated, both to the client, as well as internally, through a series of individual page designs in Photoshop, is extremely time consuming, error-prone and difficult to maintain. This is especially true on projects, that are large in scope, with many different page types and sections.

Individual page designs only has its merits on very small projects, such as a brand sites, edetailers or similar products. The key is to know the longevity of a project.

No matter the approach, a developer would need a styleguide and an asset list, consisting of:

* Navigation
* Typography and spacing (h1, h2, h3, p, table, list examples, image formatting)
* Links, buttons and their respective states, like hover, active state
* Colorschemes and consistent use of gradients
* Horizontal and vertical grids
* Pagination
* Iconography
* Different content modules, i.e image galleries, sliders and view types (i.e article lists)
* Form elements, i.e input types, selects, checkboxes and so forth

*If these are located in several page designs, please attach a document explaining where to locate each of these elements. But try to keep it all in a single PSD file.*

If there are no form elements or icons, for instance, these can of course be omitted.

If there's a complete and up to date wireframe present, the developer only needs to see a few example pages of the elements in use, i.e a frontpage, a typical article page or similar. If the asset list is maintained and properly assembled, any new pages can be quickly setup with already existing elements. The key is being able to reuse components throughout the project.

## Complete list of deliverables on new projects

* Asset list and styleguides
* Wireframes
* Individual page designs if needed
* All icons should be delivered as individual SVG files, which makes it easier to implement (512x512px)
* Fonts to be used in the product, be it an iOS application, web page or otherwise. Make sure the licensing are in order, so the developer doesn't have to spend needless time hunting down information
* Specification or brief, with page numbers and a table of contents

## Design specific
All Photoshop documents should be cleaned up, layers should be located in appropriately named folders, and all graphics should be in vector, so the developer can extract them as needed, both for a regular screen as well as retina.

Let the designer go through their deliverables with the [following](http://photoshopetiquette.com "Photoshop Etiquette") in mind.

#






