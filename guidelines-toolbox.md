# Toolbox

## Development

We use a variety of different tools in our development environment, be it .NET based or otherwise.

Projects handled completely by the front-end team is mostly based on our boilerplate, which in turn has support for Grunt, Yeoman and Bower.

.NET based projects differs from project to project, but most of them doesn't support these tools. Instead they utilize r.js or compass directly, but a lot of the same concepts still applies - Like r.js configuration options.

These are some of the tools we require you to use:

### Node.js

Node.js is used to power our development services and build tools, while also powering a small subset of sites in production.

You can download and install node.js from here: [http://nodejs.org](http://nodejs.org)

### Ruby

Ruby and Compass by extension is deprecated internally (01-01-2016), but certain legacy sites still use it.

Download the latest Ruby 2.1.x installer and install it. [http://rubyinstaller.org/downloads/]

#### Compass

Install compass in a ruby prompt:

	gem update --system
	gem install compass

### Bower

As a front-end package manager we utilize [Bower](https://github.com/bower/bower). It's a fairly standard package manager, resembling _npm_.

It also depends on node and npm which is used to install it. When they're installed, you can proceed to install Bower like this:

	npm install -g bower

You can read more about Bower and its use right here: [https://github.com/bower/bower](https://github.com/bower/bower).

## Build and deployment

### Gulp & Grunt

We use Gulp to handle our development server and build process. Grunt is deprecated internally, but are still being used for a few legacy projects.

You install them by running the following command:

	npm install -g gulp
	npm install -g grunt-cli

The file named gulpfile.js (Grunt: Gruntfile.js), which is located in the root folder of a project, contains all the different configuration for either of them. It differs from project to project, so feel free to look at the example included in the boilerplate.

## Version control
* [Git](http://git-scm.com/) & [BitBucket](http://bitbucket.org)

## Setting up an existing project

This is a fairly easy task, as long as grunt and bower is installed.

Clone the wanted repository from Bitbucket.

Run `npm install` from the root folder of the app. When that completes, you only need to run `bower install` and everything should automatically update itself.

You're now ready to develop! Launch a node.js based dev server by typing `gulp`.
