# Vertic - Front-end development guidelines

## Friendly guidelines

It's important to stay flexible, and on top of different technologies. Making sure that deploy times stay minimal, that code stays consistent and readable, and that the build/compile phase is as smooth as possible. These are the end goals of this documentation repository. This is in no way a final version; these documents are bound to change as long as we do.

We're front-end developers. We take wireframes, sketches, mockups and fully fledged designs and ___bring them to life___. That's our job and our responsibility. Being a front-end developer might have grown increasingly complicated during the last couple of years. But our toolset is supposed to make our job easier, not slow it down or hinder further experiementation. Keep this in mind.

## Workflow

More often than not, we're the last people to see, touch and tinker with a design before a project is released to the users. So, remember that:

* The design is your friend. Make sure that it shines.
* The UI Designer is your friend. Show them your work regularly, talk about changes and challenges. Tell them if they're using a gazillion different font-sizes and you'd like to change it. Bring them a croissant.
* The UX Designer is your friend. Show them your work regularly, get their thoughts about transitions and animations, feel and responsiveness. Show them your nifty CSS transitions and let them know, when it only works in Webkit. Have a coffee with them.
* The Project Manager is your friend. Help them understand what makes your work easy, and what doesn't. Give them updates on your progress. Sometimes they bring candy.
* Do great work. Be proud of it.

## Coding and methodology guidelines

Having a collection of maintained code guidelines serves as an invaluable tool to make sure everyone can read and understand each others code. It makes for much easier transitions when a new developer has to collaborate on a project if these common guidelines are followed as closely as possible. And it goes beyond that, as having these guidelines makes it a lot easier to ensure the needed level of code quality when working with external developers.

* [CSS Guidelines](https://bitbucket.org/verticlabs/vertic-documentation/src/eb5ff3e5008e4b470437f8b3bad4f2db6c3e3044/guidelines-css.md?at=master&fileviewer=file-view-default)
* [JavaScript Guidelines](https://bitbucket.org/verticlabs/vertic-documentation/src/eb5ff3e5008e4b470437f8b3bad4f2db6c3e3044/guidelines-js.md?at=master&fileviewer=file-view-default)

You can find information on how to setup your development environment here:

* [Toolbox & Setup](/verticlabs/vertic-documentation/src/984922c302fb959e5ab74f82bcd06040d22d912c/guidelines-js.md?at=master&fileviewer=file-view-default)

## Technology guides

We use a wide range of (third party) frameworks, libraries and other "helpers" in most of our day-to-day work. It pays to be familiar with these before editing a project. Most projects will speciify in their readme what technologies and tools are involved and highlight if any of those used aren't part of our usual toolbox. But in most projects, you'll most likely see:

* __[SASS](http://sass-lang.com/)__  
* __[Compass](http://compass-style.org/)__
* __[jQuery 1.x](http://api.jquery.com/)__  
  We use jQuery 1.x in most of our work. We haven't moved to 2.x yet, as almost all our projects require more backwards browser support than 2.x gives us. A good rule of thumb to be sure to look at the same version of the documentation as the one the current project is using. For instance, the [documentation for jQuery 1.9 is here](http://api.jquery.com/category/version/1.9/).
* __[Modernizr](http://modernizr.com/)__
* __[RequireJS](http://http://requirejs.org//)__  
  RequireJS is a great dependency (AMD) loader that allows us to keep our JS modular.
* __[Underscore.js](http://underscorejs.org/)__  
  Underscore provides a nifty sit of utilities for common JS tasks. you'll mostly see this in [Backbone.js](http://backbonejs.org/) projects or other projects that doesn't have jQuery.
